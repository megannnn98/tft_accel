/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "pt-1.4/pt.h"
#include <arm_math.h>
#include "tft.h"
#include "tft_gfx.h"
#include "common_types.h"
#include "messages.h"
#include "ring.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
/* USER CODE BEGIN PFP */
#define FFT_SIZE 256
#define ARRAY_SIZE FFT_SIZE

uint8_t BSP_ACCELERO_Init(void);
void BSP_ACCELERO_GetXYZ(int16_t *pDataXYZ);
        void ACCELERO_IO_Read(uint8_t* pBuffer, uint8_t ReadAddr, uint16_t NumByteToRead);
        
        
ring32f_t ring32f;
//static int j = 0;
static struct {
    uint8_t normalized_output_buffer[ARRAY_SIZE];
    uint8_t last_buffer[ARRAY_SIZE];
} = {0};

u32 r32 = 0, q32 = 0, m32 = 0, n32 = 0;
u8 sums_ready = 0;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


u16 segment_sums[8] = {0};
u16 cur_segment_sums[8] = {0};

void draw_column(u8 x_pos, u8 y0, u8 height, u16 color) 
{
    u8 start, finish;
    u16 t_color;
        
    if (y0 == height) {
        return;
    }

    start = (y0 < height) ? y0 : height;
    finish = (y0 < height) ? height : y0;
    t_color = (y0 < height) ? ILI9340_YELLOW : ILI9340_BLACK;
    
    for(u8 k = start; k < finish; k++) {
        tft_drawPixel(x_pos, k, t_color);
    } 

//    tft_drawLine(x_pos, 0, x_pos, height, color);
}

void tft_draw_array(u8* new_buff, u8* old_buff, u16 len, u16 color)
{

    for(int i = 0; i < len; i++) {
        draw_column(i, old_buff[i], new_buff[i], color);
    }  
    
}

PT_THREAD (receive_accel_thread(pt_t* pt)) 
{
    PT_BEGIN(pt);

    static float32_t rms_square_root;
    static uint16_t t16 = 0;
    static int16_t buffer[3];
    static arm_status status; 
    static arm_cfft_radix4_instance_f32 S; 
    static float32_t tmp_input[FFT_SIZE];
    static float32_t output[FFT_SIZE];
    static float32_t maxValue;
    static uint32_t index_max, index_min;
    static u8 acc_status;

    /* Initialize the CFFT/CIFFT module */  
    status = arm_cfft_radix4_init_f32(&S, FFT_SIZE,0, 0);
    if (ARM_MATH_SUCCESS != status) {
        while(1);
    }

    
    while(1)
    {
        PT_YIELD(pt);
        

        ACCELERO_IO_Read(&acc_status, 0x27, 1);
        
        if (0x0f != (acc_status & 0x0f)) {
            continue;
        }
        
        /* Read Acceleration*/
        BSP_ACCELERO_GetXYZ(buffer);

        arm_sqrt_f32((buffer[0]*buffer[0] + buffer[1]*buffer[1] + buffer[2]*buffer[2]), &rms_square_root);
        ring_put_32f(&ring32f, rms_square_root);

        if (ring_get_cnt_32f(&ring32f) == FFT_SIZE) {
            t16++;
            
            if (0 == (t16 % 8) ) {                    
                memcpy(&tmp_input, ring32f.array, FFT_SIZE*sizeof(float));
                arm_cfft_radix4_f32(&S, tmp_input); 
                arm_cmplx_mag_f32(tmp_input, output, FFT_SIZE);  
                arm_max_f32(output, FFT_SIZE, &maxValue, &index_max);
                arm_min_f32(output, FFT_SIZE, &maxValue, &index_min);
                output[index_max] = output[index_min];
                arm_max_f32(output, FFT_SIZE, &maxValue, &index_max);
                
                for (int i = 0; i < FFT_SIZE; i++) {                
                    normalized_output_buffer[i] = (u8) (128.*(output[i] / maxValue));
                }
                memset(output, 0, FFT_SIZE*sizeof(float32_t));
                
                msg.set(SHOW_GRAPH_MSG, NULL, 0);
                q32 = HAL_GetTick() - r32;
                r32 = HAL_GetTick();
            }   
        }  
        
        
        
    }// while(1)

    
    PT_END(pt);
}

void get_segments_vals(u16 out[], u8 const in[])
{
    static u16 t16;
    static u8 i, j;
    
    for (t16 = 0, i = 0; i < 8; i++) {
        out[i] = 0;
        for (j = 0; j < 16; j++) {
            out[i] += in[t16++];
        }
        out[i] >>= 4;
    }
}

int compare_segments(u16 const in1[], u16 const in2[])
{
    int cnt = 0;
    for (int i = 0; i < 8; i++) {
        cnt += abs(cur_segment_sums[i] - segment_sums[i]);
    }
    return cnt;
}


PT_THREAD (tft_fft_thread(pt_t* pt)) 
{
    PT_BEGIN(pt);
    
    static int diff = 0, max_diff = 0;
    
    while(1) {
      
        PT_YIELD(pt);        
        
        if (msg.getVal(SHOW_GRAPH_MSG)) {
          
            m32 = HAL_GetTick();
          
            tft_draw_array(normalized_output_buffer, last_buffer, ARRAY_SIZE/2, ILI9340_YELLOW);            
            memcpy(last_buffer, normalized_output_buffer, ARRAY_SIZE/2);
            
            if (sums_ready) {
                get_segments_vals(cur_segment_sums, normalized_output_buffer);
                
                diff = compare_segments(cur_segment_sums, segment_sums);
                max_diff = (diff > max_diff) ? diff : max_diff;
            }
            
            if (msg.getVal(BTN_MSG) && !sums_ready) {
                sums_ready = 1;
                
                get_segments_vals(segment_sums, normalized_output_buffer);
            }
            
            
            n32 =  HAL_GetTick() - m32;
        }

    }// while(1)
    
    PT_END(pt);
}

PT_THREAD (btn_thread(pt_t* pt)) 
{
    PT_BEGIN(pt);
    
    static uint32_t t32 = 0;
    
    while(1) {

        PT_WAIT_WHILE(pt, BTN_NOT_PUSHED);
        t32 = HAL_GetTick() + 30;
        PT_WAIT_WHILE(pt, t32 > HAL_GetTick());  
        if (BTN_NOT_PUSHED) {
            continue;
        }
          
        msg.set(BTN_MSG, NULL, 0);

    }// while(1)
    
    PT_END(pt);
}
  
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  pt_t receive_accel_pt;
  pt_t tft_fft_pt;
  pt_t btn_pt;
  
  PT_INIT(&receive_accel_pt);
  PT_INIT(&tft_fft_pt);
  PT_INIT(&btn_pt);
      
  msg.init();
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  /* USER CODE BEGIN 2 */
  
  HAL_Delay(200);
  TFT_RESET_GPIO_Port->ODR |= TFT_RESET_Pin;
  
    /* Initialize Accelerometer MEMS */
  if (BSP_ACCELERO_Init() != HAL_OK) {
      /* Initialization Error */
      Error_Handler();
  }
  
  void LIS302DL_InterruptConfigSimple();
  LIS302DL_InterruptConfigSimple();
  
  tft_delay_func = HAL_Delay;
  TFT_led(1);
  TFT_init();
  tft_clean();
  tft_setRotation(1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    receive_accel_thread(&receive_accel_pt);
    tft_fft_thread(&tft_fft_pt);
    btn_thread(&btn_pt);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_1LINE;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, TFT_CS_Pin|TFT_A0_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, TFT_RESET_Pin|LD4_Pin|LD3_Pin|LD5_Pin 
                          |LD6_Pin|Audio_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CS_I2C_SPI_Pin */
  GPIO_InitStruct.Pin = CS_I2C_SPI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CS_I2C_SPI_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : BOOT1_Pin */
  GPIO_InitStruct.Pin = BOOT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BOOT1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : TFT_CS_Pin TFT_A0_Pin */
  GPIO_InitStruct.Pin = TFT_CS_Pin|TFT_A0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : TFT_RESET_Pin LD4_Pin LD3_Pin LD5_Pin 
                           LD6_Pin Audio_RST_Pin */
  GPIO_InitStruct.Pin = TFT_RESET_Pin|LD4_Pin|LD3_Pin|LD5_Pin 
                          |LD6_Pin|Audio_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
  GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PE1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
