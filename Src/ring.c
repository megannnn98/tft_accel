/**
  ******************************************************************************
  * @file    ring.c
  * @author  Karatanov
  * @version V1.0.0
  * @brief   Ring buff functions 
  */

/* Includes ------------------------------------------------------------------*/
#include "ring.h"
#include "cmsis_iar.h"
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Global functions ---------------------------------------------------------*/

/** @addtogroup UART
  * @{
  */
/** @addtogroup UART_Circular_Handlers
  * @{
  */


/**
  * @brief  �������� ������ � �����
  * @param  ����� UART ����������
  * @param  ����� ������ ������
  * @retval None
  */
void ring_put_32f(ring32f_t* buf, const float sym)
{

    if (buf->cnt < RING_SIZE) { //���� � ������ ��� ���� �����
        buf->array[buf->tail++] = sym; //�������� � ���� ������
        buf->cnt++; //�������������� ������� ��������
        if (buf->tail == RING_SIZE) {
            buf->tail = 0;
        }
    }
    else {
        ring_get_32f(buf);
        ring_put_32f(buf, sym);
    }
}

/**
  * @brief  ����� ������ �� ������
  * @param  ����� UART ����������
  * @retval ������� �� �������
  */
float ring_get_32f(ring32f_t* buf)
{
    float sym = 0;

    __disable_irq();
    if (buf->cnt > 0) { //���� ����� �� ������
        sym = buf->array[buf->head++]; //��������� ������ �� ������
        buf->cnt--; //��������� ������� ��������
        if (buf->head == RING_SIZE) {
            buf->head = 0;
        }
    }
    __enable_irq();
    return sym;
}

/**
  * @brief  ����� ������ �� ������, �� ������ �� ������
  * @param  ����� UART ����������
  * @retval ������� �� �������
  */
float ring_peek_32f(ring32f_t* buf, u16 shift)
{
    float sym = 0;

    __disable_irq();
    if (buf->cnt > 0) { //���� ����� �� ������
        if (buf->head + shift >= RING_SIZE) {
            shift = (buf->head + shift) - RING_SIZE;
            sym = buf->array[shift]; //��������� ������ �� ������
        }
        else {
            sym = buf->array[buf->head + shift]; //��������� ������ �� ������
        }
    }
    __enable_irq();
    return sym;
}

/**
  * @brief  ������� ��������� � ��������� ������
  * @param  ����� UART ����������
  * @retval ����������
  */
u16 ring_get_cnt_32f(ring32f_t* buf)
{
    return buf->cnt;
}
/**
  * @brief  �������� ���� ������
  * @param  ����� UART ����������
  * @param  ��������� �� ������ � ������� ��������� 
  * @retval None
  */
void ring_readout_32f(ring32f_t* buf, float p[])
{
    //static __istate_t s;
    //s = __get_interrupt_state();
    __disable_irq();
    for (u16 i = 0; buf->cnt; i++) {
        p[i] = ring_get_32f(buf);
    }
    __enable_irq();
}

/**
  * @brief  �������" �����
  * @param  ����� UART ����������
  * @retval None
  */
void ring_flush_32f(ring32f_t* buf)
{
    buf->tail = 0;
    buf->head = 0;
    buf->cnt = 0;
}


// --------------------------------------------------------------------------
