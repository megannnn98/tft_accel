


#include "messages.h"


/* Global variables ---------------------------------------------------------*/
volatile static struct {
    u8 val;
    u8* paramPtr;
    u16  paramSize;
} messages[MAX_MSG];

/* Private functions ---------------------------------------------------------*/


/** @addtogroup MSG
  * @{
  */

/**
  * @brief  ������������� ���������
  * @param  None
  * @retval None
  */
static void MSG_Init()
{
    for(u8 i = 0; i < MAX_MSG; i++)
    {
        messages[i].val = 0;
        messages[i].paramPtr = NULL;
        messages[i].paramSize = 0;
    }
}


/**
  * @brief  ���������� ��������� 
  * @param  None
  * @retval ������ ������
  */
static u8 MSG_Set(const u8 imsg, void* const iparamPtr, const u16 iparamSize)
{
    if (imsg < MAX_MSG)
    {
        messages[imsg].val       = 2;
        messages[imsg].paramPtr  = (u8*)iparamPtr;
        messages[imsg].paramSize = iparamSize;
        return (u8)0;
    }
    return (u8)-1;
}

/**
  * @brief  ���������� ��������� 
  * @param  None
  * @retval ������ ������
  */
static u8 MSG_Repeat(const u8 imsg)
{
    if (imsg < MAX_MSG)
    {
        messages[imsg].val = 2;
        return (u8)0;
    }
    return (u8)-1;
}

/**
  * @brief  �������� ��������� 
  * @param  ����� ��������� ���������
  * @retval ���� ��������� ��� ���
  */
static bool getMsgVal(const u8 imsg)
{
    if(messages[imsg].val > 0)
    {
        messages[imsg].val = 0;
        return true;
    }
    return false;
}

/**
  * @brief  �������� ��������� �� ��������� 
  * @param  ����� ��������� ���������
  * @retval ��������� 
  */
static bool peekMsgVal(const u8 imsg)
{
    if(messages[imsg].val > 0) {
        return true;
    }
    return false;
}

/**
  * @brief  ???????? ????????? ?? ????????? 
  * @param  ????? ????????? ?????????
  * @retval ????????? 
  */
static void* getMsgPtr(const u8 imsg)
{
    return messages[imsg].paramPtr;
}
/**
  * @brief  �������� ������ ���������
  * @param  ����� ��������� ���������
  * @retval ������
  */
static u16 getMsgSize(const u8 imsg)
{
    return messages[imsg].paramSize;
}

/**
  * @brief  ������� �������������� ��������� 
  * @param  None
  * @retval None
  */
static void process_msg()
{
    static u8 i;  
    
    // �� ���� ���������
    // ���� ����� �� ������ ����, ����������� ��� �������
    // ���� ���� ������� ������� �� 32 �������� - ��������� ����� �� ���� �������
    for (i = 0; i< MAX_MSG; i++) 
    {
      if (messages[i].val > 0) 
      {
          messages[i].val--;
      }
    }
}



/**
  * @brief  MSG_Driver.
*/
const struct Msg_driver msg = 
{
  .init    = MSG_Init,
  .process = process_msg,
  .set     = MSG_Set,
  .repeat  = MSG_Repeat,
  .getVal  = getMsgVal,
  .peekVal = peekMsgVal,
  .getPtr  = getMsgPtr,
  .getSize = getMsgSize,
  .clear   = getMsgVal,
};

/**
  * @}
  */

