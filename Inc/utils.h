

#pragma once

/* Includes ------------------------------------------------------------*/

#include <stdbool.h>
#include "pt.h"
#include "HAL.h"
#include "common_types.h"
#include "uart.h"
#include "config.h"

/* Global define ------------------------------------------------------------*/

#if DEVICE_TYPE == 1
#define THREE_LEDS 1
#define MUX_ON 0
#elif DEVICE_TYPE == 2
#define THREE_LEDS 0
#define MUX_ON 0
#elif DEVICE_TYPE == 3
#define THREE_LEDS 1
#define MUX_ON 1
#else
#error "wrong type"
#endif 



#define UART_TIMEOUT (1000U)
#define MAX_SIZE_HDLC 128

/* Global define ------------------------------------------------------------*/
enum {
    MS_IN_SEC = (1000UL),
    MS_IN_MIN = (60 * MS_IN_SEC),
    MS_IN_HOUR = (60 * MS_IN_MIN),
};

#define PPPINITFCS16 0xffff /* ��������� �������� FCS */
#define PPPGOODFCS16 0xf0b8 /* �������� �������� FCS */

/* Global variables ---------------------------------------------------------*/

/* Global define ------------------------------------------------------------*/

#define CALIBR_Port GPIOB
#define CALIBR_PIN_8 GPIO_PIN_8

#define MAX_RING (3)
#define PORT_SIZE (6)

enum {
    REQUEST = 0,
    PROCESS,
};

enum {
    GENERAL_TIMER = 0,
    GSM_TIMER,
    METER_TIMER,
    OPTO_TIMER,
    SERVICE_TIMER,

    MAX_TIMERS
};

enum {
    GENERAL_MSG = 0,

    //  GSM_TO_METER_CONNECT_GPRS_MSG,
    //  METER_TO_GSM_CONNECT_GPRS_MSG,

    //  GSM_TO_OPTO_DISC_GPRS_MSG,
    //  OPTO_TO_GSM_DISC_GPRS_MSG,
    //  GSM_TO_OPTO_CONNECT_GPRS_MSG,
    //  OPTO_TO_GSM_CONNECT_GPRS_MSG,

    METER_TO_GSM_CSD_MSG,
    GSM_TO_METER_CSD_MSG,

    //  OPTO_TO_GSM_GPRS_MSG,
    //  GSM_TO_OPTO_GPRS_MSG,
    //  GSM_TO_OPTO_GPRS_BREAK_MSG,
    OPTO_TO_GSM_CSD_MSG,
    GSM_TO_OPTO_CSD_MSG,

    GSM_RXNE_MSG,
    OPTO_RXNE_MSG,
    METER_RXNE_MSG,
    SERVICE_RXNE_MSG,

    GSM_CLEAR_MSG,
    OPTO_CLEAR_MSG,
    METER_CLEAR_MSG,
    SERVICE_CLEAR_MSG,

    STOP_APN_WRITE_MSG,
    GSM_CLIENT_START_MSG,
    GSM_SMS_RXED_MSG,
    GSM_SMS_WAIT_FOR_CONFIG_MSG,

    GSM_SMS_INITIATIVE_MSG,
    GSM_SMS_INITIATIVE_BLINK_MSG,

    GSM_IS_ALIVE_SERV_MSG,
    SOFT_RESET_MSG,
    GSM_CLIENT_SET_TIMEOUT_MSG,
    
    OPTO_PKT_RXED_MSG,
    METER_PKT_RXED_MSG,
    
    MEM_WRITE_START_MSG,
    MEM_WRITE_READY_MSG,

    MAX_MSG
};

enum {
    MEM_MYNUM = 0,
    MEM_APN1, // 1
    MEM_APN2, // 2
    MEM_IP, // 3
    MEM_PASS, // 4
    MEM_SMS_NUM0, // 5
    MEM_SMS_NUM1, // 6
    MEM_SMS_NUM2, // 7
    MEM_SMS_NUM3, // 8
    MEM_UART, // 9
    MEM_CRLF, // 10
    MEM_PORT, // 11
    MEM_MAX
};

enum {
    SERIAL_ERR_FUNC = 1,
    SERIAL_ERR_LEN = 2,
    SERIAL_ERR_CRC = 3,
    SERIAL_ERR_LONG_LEN = 4,
    SERIAL_ERR_MEM = 5,
    SERIAL_ERR_DATA = 6,
    SERIAL_ERR_TIMEOUT = 7,
};

enum {
    PR_MAX = 0,
    PR_HIGH,
    PR_MEDIUM,
    PR_LOW,
    PR_LOWEST,
};

#define CALIBR_ON   (true == calibr_on)
#define CALIBR_OFF  (!CALIBR_ON)

#define PR_MIN_RATE (16)

#define FIRMWARE_VERSION ((u16)0x0105)

typedef __packed struct {
    u16 start_addr;
    u16 len;
    u8 data[];
} mem_pkt_t;


/* Global macro -------------------------------------------------------------*/
#ifndef PRINT

#define PRINT(...)                                                                      \
do {                                                                                    \
    extern char debug_str[];                                                            \
    extern UART_HandleTypeDef H_SERVICE;                                                \
    extern bool calibr_on;                                                              \
    if ((DEBUG) && (CALIBR_OFF)) {                                                      \
        sprintf(debug_str, __VA_ARGS__);                                                \
        HAL_UART_Transmit(&H_SERVICE, (uint8_t*)" ", 1, 0x1000);                        \
        HAL_UART_Transmit(&H_SERVICE, (uint8_t*)debug_str, strlen(debug_str), 0x1000);  \
        HAL_UART_Transmit(&H_SERVICE, (uint8_t*)" ", 1, 0x1000);                        \
    }                                                                                   \
} while (0)
#endif

#ifndef PRINT_ARRAY
#define PRINT_ARRAY( text1, p_, tmp16, text2)  PRINT(text1); for (int k = 0; k < tmp16; k++) {PRINT("%02X ", p_[k]);}PRINT(text2);
#endif

#define PT_DELAY_MS(timer, ms)                  \
    do {                                        \
        tim.reset(timer);                       \
        PT_WAIT_WHILE(pt, tim.get(timer) < ms); \
    } while (0)

#define PT_STATE_INIT()              \
    static pt_t val_pt = {.lc = 0 }; \
    static pt_t* pt = &val_pt;

#define BYTE_0(n) ((uint8_t)((n) & (uint8_t)0xFF)) /*!< Returns the low byte of the 32-bit value */
#define BYTE_1(n) ((uint8_t)(BYTE_0((n) >> (uint8_t)8))) /*!< Returns the second byte of the 32-bit value */
#define BYTE_2(n) ((uint8_t)(BYTE_0((n) >> (uint8_t)16))) /*!< Returns the third byte of the 32-bit value */
#define BYTE_3(n) ((uint8_t)(BYTE_0((n) >> (uint8_t)24))) /*!< Returns the high byte of the 32-bit value */

#define PT_YIELD_SPAWN(pt)        \
    do {                          \
        PT_YIELD_FLAG = 0;        \
        LC_SET((pt)->lc);         \
        if (PT_YIELD_FLAG == 0) { \
            return PT_WAITING;    \
        }                         \
    } while (0)

#define PT_EXIT_OK(pt)       \
    do {                     \
        PT_INIT(pt);         \
        return PT_EXITED_OK; \
    } while (0)

#define PT_EXIT_BAD(pt)       \
    do {                      \
        PT_INIT(pt);          \
        return PT_EXITED_BAD; \
    } while (0)

#define PT_EXIT_TIMEOUT(pt)       \
    do {                          \
        PT_INIT(pt);              \
        return PT_EXITED_TIMEOUT; \
    } while (0)

#define PT_EXIT_BREAK(pt)       \
    do {                        \
        PT_INIT(pt);            \
        return PT_EXITED_BREAK; \
    } while (0)

#define PT_YIELD_WAIT(pt)         \
    do {                          \
        PT_YIELD_FLAG = 0;        \
        LC_SET((pt)->lc);         \
        if (PT_YIELD_FLAG == 0) { \
            return PT_WAITING;    \
        }                         \
    } while (0)

#define PT_YIELD_MAIN(pt) PT_YIELD(pt);

#define ATOMIC_START() \
    do {               \
    __disable_irq()
#define ATOMIC_STOP() \
    __enable_irq();   \
    }                 \
    while (0)

#define SWAP_8(a1, a2, tmp) \
    do {                    \
        tmp = a1;           \
        a1 = a2;            \
        a2 = tmp;           \
    } while (0)

#define IS_SUBSTR(str, substr) (NULL != (strstr((char*)str, substr)))


#define HTONS(x) (((x >> 8) & 0x00FF) | ((x << 8) & 0xFF00))
#define HTONL(x) (((x >> 24) & 0x000000FF) | ((x >> 8) & 0x0000FF00) | ((x << 8) & 0x00FF0000) | ((x << 24) & 0xFF000000))

/*
 * BE conversion
 */

#define htons(a) ((((a) >> 8) & 0xff) | (((a) << 8) & 0xff00))
#define ntohs(a) htons(a)

#define htonl(a) ((((a) >> 24) & 0xff) | (((a) >> 8) & 0xff00) | (((a) << 8) & 0xff0000) | (((a) << 24) & 0xff000000))
#define ntohl(a) htonl(a)
      
/* Global functions  ---------------------------------------------------------*/

