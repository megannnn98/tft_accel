
#pragma once 
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "common_types.h"

#define RING_SIZE 256


#define RING_BUF(typename_, uint_size, array_size) \
typedef struct { \
    uint_size array[array_size]; \
    u16 head; \
    u16 tail; \
    u16 cnt; \
} typename_;

RING_BUF(ring32f_t, float, RING_SIZE);


extern ring32f_t ring32f;

void ring_put_32f(ring32f_t* buf, const float sym);
float ring_get_32f(ring32f_t* buf);
float ring_peek_32f(ring32f_t* buf, u16 shift);
float ring_get_head_32f(ring32f_t* buf);
u16 ring_get_cnt_32f(ring32f_t* buf);
void ring_readout_32f(ring32f_t* buf, float p[]);
void ring_flush_32f(ring32f_t* buf);