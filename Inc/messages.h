

#pragma once

/* Includes ------------------------------------------------------------*/

#include <stdbool.h>
#include "common_types.h"

enum {
    GENERAL_MSG = 0,
    ACC_XYZ_ISR_MSG,
    SHOW_GRAPH_MSG,
    BTN_MSG,
    
    MAX_MSG
};


/**
  * @{ @brief  Структура дравера MSG.
*/
struct Msg_driver {
    void (*init)();
    void (*process)();
    u8 (*set)(const u8 imsg, void* iparamPtr, u16 iparamSize);
    u8 (*repeat)(const u8 imsg);

    bool (*peekVal)(const u8 imsg);
    bool (*getVal)(const u8 imsg);
    void* (*getPtr)(const u8 imsg);
    u16 (*getSize)(const u8 imsg);
    bool (*clear)(const u8 imsg);
};
/**
  * @}
  */

extern const struct Msg_driver msg;

